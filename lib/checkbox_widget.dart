import 'package:flutter/material.dart';

Widget _checkBox(String title, bool value, onChanged) {
  return Container(
      padding: EdgeInsets.only(left: 16, right: 16, bottom: 8),
      child: Column(
        children: [
          Row(
            children: [
              Text(title),
              Expanded(child: Container()),
              Checkbox(value: value, onChanged: onChanged)
            ],
          ),
          Divider(),
        ],
      ));
}

class CheckboxWidget extends StatefulWidget {
  const CheckboxWidget({Key? key}) : super(key: key);

  @override
  _CheckboxWidgetState createState() => _CheckboxWidgetState();
}

class _CheckboxWidgetState extends State<CheckboxWidget> {
  bool check1 = false;
  bool check2 = false;
  bool check3 = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Checkbox')),
      body: ListView(
        children: [
          _checkBox('check1', check1, (value) {
            setState(() {
              check1 = value!;
            });
          }),
          _checkBox('check2', check2, (value) {
            setState(() {
              check2 = value!;
            });
          }),
          _checkBox('check3', check3, (value) {
            setState(() {
              check3 = value!;
            });
          }),
          TextButton(
            child: Text('Save'),
            onPressed: () {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Text(
                      'check1: $check1,check2: $check2, check3: $check3')));
            },
          ),
        ],
      ),
    );
  }
}
